# Apache Solr Buckets

## Install instructions

It is expected that you have Search Api Solr enabled. For now it's only built for that.
Just enable the module and it should be ready to use on the Search Api Solr indexes.



## Usage

You can find the functionality on the index page, where you usually index content, in the "Apache Solr Buckets fieldset".
* Click the "Index it" button.
* Add bucket(s).
* Wait while indexing.

Up to 6 buckets are allowed, since that's the limit of concurrent requests of most browsers.

It will index all items in the index, and mark them indexed.

Option to only index pending items will come at some point.
