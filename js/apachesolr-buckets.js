(function($) {
  Drupal.apachesolrBuckets = Drupal.apachesolrBuckets || {};

  /**
   * [apachesolrBuckets description]
   * @param  {[type]} context  [description]
   * @param  {[type]} settings [description]
   * @return {[type]}          [description]
   */
  Drupal.admin.behaviors.apachesolrBuckets = function (context, settings) {
    settings = settings || Drupal.settings;

    var initialized = initialized || settings.apachesolrBuckets.initialized;

    if (!initialized) {
      Drupal.apachesolrBuckets.init(settings.apachesolrBuckets);
    }

    Drupal.settings.apachesolrBuckets.windowTitle = window.document.title;
    Drupal.settings.apachesolrBuckets.initialized = true;
  };

  /**
   * [init description]
   * @param  {[type]} settings [description]
   * @return {[type]}          [description]
   */
  Drupal.apachesolrBuckets.init = function(settings) {
    settings.itemsProcessed = 0;
    settings.bucketProcessing = 0;
    settings.itemsSkipped = 0;
    settings.itemsLeft = settings.itemsCount;
    settings.timeSpent = 0;
    settings.timeLeft = 0;
    settings.timeSpentSum = 0;

    settings.finished = false;

    settings.offset = 0;

    settings.bucketsActive = 0;

    settings.startTime = 0;
    settings.finishTime = '-';

    $('.js-button-add').bind('click', function() {
      Drupal.apachesolrBuckets.addBucket();
    });

    $('.js-button-remove').bind('click', function() {
      Drupal.apachesolrBuckets.removeBucket();
    });

    $("#js-items-per-bucket-slider").slider({
      range: "min",
      value: Drupal.settings.apachesolrBuckets.limit,
      step: 50,
      min: 50,
      max: 1000,
      slide: function(event, ui) {
        Drupal.settings.apachesolrBuckets.limit = ui.value;
        $('#js-limit').html(ui.value);
      }
    });

    Drupal.apachesolrBuckets.buckets = new Array();
    $('.bucket').each(function () {
      Drupal.apachesolrBuckets.buckets.push($(this));
    });

    Drupal.apachesolrBuckets.updateUI();
  }

  Drupal.apachesolrBuckets.addBucket = function () {
    if (Drupal.settings.apachesolrBuckets.bucketsActive > 5) {
      return;
    }

    if (Drupal.settings.apachesolrBuckets.finished) {
      return;
    }

    if (!settings.startTime) {
      Drupal.settings.apachesolrBuckets.startTime = (new Date());
    }

    Drupal.apachesolrBuckets.buckets[Drupal.settings.apachesolrBuckets.bucketsActive].toggleClass('bucket-active', true);
    Drupal.apachesolrBuckets.buckets[Drupal.settings.apachesolrBuckets.bucketsActive].toggleClass('bucket-removing', false);

    Drupal.settings.apachesolrBuckets.bucketsActive++;

    Drupal.apachesolrBuckets.runBucket(Drupal.settings.apachesolrBuckets.bucketsActive);

    Drupal.apachesolrBuckets.updateUI();
  }

  Drupal.apachesolrBuckets.removeBucket = function () {
    if (Drupal.settings.apachesolrBuckets.bucketsActive < 1) {
      return;
    }

    if (Drupal.settings.apachesolrBuckets.finished) {
      return;
    }

    Drupal.settings.apachesolrBuckets.bucketsActive--;
    Drupal.apachesolrBuckets.buckets[Drupal.settings.apachesolrBuckets.bucketsActive].toggleClass('bucket-active', false);
    Drupal.apachesolrBuckets.buckets[Drupal.settings.apachesolrBuckets.bucketsActive].toggleClass('bucket-removing', true);

    Drupal.apachesolrBuckets.updateUI();
  }

  Drupal.apachesolrBuckets.runBucket = function (bucketId) {
    // Generate the url.
    var limit = Drupal.settings.apachesolrBuckets.limit;

    var url = Drupal.settings.apachesolrBuckets.bucketCallback
 + '?limit=' + limit
 + '&offset=' + Drupal.settings.apachesolrBuckets.offset
 + '&max_item_id=' + Drupal.settings.apachesolrBuckets.maxItemId
 + '&bucket_id=' + bucketId;
    for (x in Drupal.settings.apachesolrBuckets.bundles) {
      url += '&bundles[]=' + Drupal.settings.apachesolrBuckets.bundles[x];
    }

    Drupal.settings.apachesolrBuckets.bucketProcessing++;

    Drupal.apachesolrBuckets.buckets[(bucketId - 1)].html(Drupal.settings.apachesolrBuckets.bucketProcessing);

    Drupal.settings.apachesolrBuckets.offset += limit;

    try {
      $.ajax({
        type: "POST",
        url: url,
        success: function(data) {
          Drupal.apachesolrBuckets.recieveBucket(data, bucketId);
        },
        dataType: 'json'
      });
    }
    catch (err) {
      return false;
    }
  }

  Drupal.apachesolrBuckets.recieveBucket = function(data, bucketId) {
    Drupal.settings.apachesolrBuckets.itemsProcessed += data.items_found;
    Drupal.settings.apachesolrBuckets.itemsSkipped += data.items_found - data.items_indexed;
    Drupal.settings.apachesolrBuckets.timeSpentSum += data.time_spent;

    $('.js-memory-peak').html(data.memory_peak);

    Drupal.apachesolrBuckets.updateUI();

    // If the bucket has been disabled, then remove classes and return.
    if (Drupal.settings.apachesolrBuckets.bucketsActive < bucketId) {
      Drupal.apachesolrBuckets.buckets[bucketId - 1].toggleClass('bucket-active', false);
      Drupal.apachesolrBuckets.buckets[bucketId - 1].toggleClass('bucket-removing', false);
      Drupal.apachesolrBuckets.buckets[bucketId - 1].html('');
      return;
    }

    // There was less items found than the limit, which means that we are done.
    if (data.items_found != data.limit) {
      Drupal.settings.apachesolrBuckets.finished = true;
      Drupal.settings.apachesolrBuckets.itemsLeft = 0;
      Drupal.apachesolrBuckets.buckets[bucketId - 1].toggleClass('bucket-active', false);
      Drupal.apachesolrBuckets.buckets[bucketId - 1].toggleClass('bucket-removing', false);
      Drupal.apachesolrBuckets.buckets[bucketId - 1].html('Finished');

      $('.js-button-add').css('cursor', 'not-allowed');
      $('.js-button-remove').css('cursor', 'not-allowed');

      return;
    }

    // Start another bucket.
    Drupal.apachesolrBuckets.runBucket(bucketId);
  }

  /**
   * [updateUI description]
   * @return {[type]} [description]
   */
  Drupal.apachesolrBuckets.updateUI = function () {
    settings = Drupal.settings.apachesolrBuckets;

    if (settings.startTime) {
      settings.timeSpent = (new Date().getUTCSeconds()) - settings.startTime.getUTCSeconds();
    }

    if (settings.startTime && settings.bucketsActive) {
      var timeLeft = Math.round((settings.itemsCount - settings.itemsProcessed) * settings.timeSpentSum / settings.itemsProcessed / 1000 / settings.bucketsActive);
      settings.timeLeft = timeLeft + ' seconds';

      var finishTime = new Date();
      finishTime.setSeconds(finishTime.getSeconds() + timeLeft);
      settings.finishTime = finishTime.toLocaleTimeString();
    }
    else {
      settings.timeLeft = 'No active buckets';
      settings.finishTime = 'No active buckets';
    }

    if (settings.itemsLeft) {
      settings.itemsLeft = settings.itemsCount - settings.itemsProcessed;
    }

    //$('.js-items-processed').html(settings.itemsCount);
    $('.js-items-processed').html(settings.itemsProcessed);
    $('.js-items-skipped').html(settings.itemsSkipped);
    $('.js-items-left').html(settings.itemsLeft);
    $('#js-limit').html(settings.limit);
    if (settings.timeSpent) {
      $('.js-time-spent').html(settings.timeSpent);
    }
    if (settings.timeLeft) {
      $('.js-time-left').html(settings.timeLeft);
    }
    if (settings.timeSpentSum) {
      $('.js-time-spent-sum').html(Math.round(settings.timeSpentSum / 1000));
    }

    if (settings.startTime) {
      $('.js-time-started').html(settings.startTime.toLocaleTimeString());
    }
    if (settings.finishTime) {
      $('.js-time-finish').html(settings.finishTime);
    }

    var percentage = 0;
    if (settings.itemsCount) {
      percentage = Math.round(100 * 100 * settings.itemsProcessed / settings.itemsCount) / 100;
    }
    $('.progress-bar').css('width', percentage + '%');
    $('.progress-percentage').html(percentage);

    if (Drupal.settings.apachesolrBuckets.initialized) {
      window.document.title = (Math.round(percentage * 10) / 10) + '% ' + Drupal.settings.apachesolrBuckets.windowTitle;
    }
  }

})(jQuery);
