<?php
/**
 * @file
 * The bucket indexing page.
 */
$row = 0;
?>
<div class="progress-wrapper">
  <div class="progress-box">
    <div class="progress-percentage">0</div>
    <div class="progress-bar"></div>
  </div>
</div>
<div class="apachesolr-buckets-container">

  <div class="stats-wrapper">
    <table class="stats-list">
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label">Index:</th><td class="stat-value"><?php print check_plain($index_label); ?></td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label odd">Max id:</th><td class="stat-value odd"><?php print check_plain($max_item_id); ?></td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label">Total items:</th><td class="stat-value"><?php print check_plain($documents_count); ?></td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label odd">Items processed per bucket:</th><td class="stat-value odd"><div id="js-items-per-bucket-slider"></div><div id="js-limit">-</div></td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label">Latest memory peak usage per bucket:</th><td class="stat-value js-memory-peak">-</td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label">Bundles:</th><td class="stat-value"><?php print check_plain(implode(', ', $bundles)); ?></td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label odd">Items processed:</th><td class="stat-value odd js-items-processed">-</td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label">Items skipped:</th><td class="stat-value js-items-skipped">-</td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label odd">Items left:</th><td class="stat-value odd js-items-left">-</td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label">Started:</th><td class="stat-value js-time-started">-</td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label">Estimated finish:</th><td class="stat-value js-time-finish">-</td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label">Time spent:</th><td class="stat-value js-time-spent">-</td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label odd">Time estimated left:</th><td class="stat-value odd js-time-left">-</td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label">Time spent buckets sum:</th><td class="stat-value js-time-spent-sum">-</td>
      </tr>
      <tr class="<?php if ($row++ % 2) : print 'odd'; else : print 'even'; endif; ?>">
        <th class="stat-label odd">Buckets:</th>
        <td class="stat-value odd">
    <div class="buckets">
      <div class="bucket js-bucket-1" data-bucket-id="1" data-bucket-status="ready"></div>
      <div class="bucket js-bucket-2" data-bucket-id="2" data-bucket-status="ready"></div>
      <div class="bucket js-bucket-3" data-bucket-id="3" data-bucket-status="ready"></div>
      <div class="bucket js-bucket-4" data-bucket-id="4" data-bucket-status="ready"></div>
      <div class="bucket js-bucket-5" data-bucket-id="5" data-bucket-status="ready"></div>
      <div class="bucket js-bucket-6" data-bucket-id="6" data-bucket-status="ready"></div>
    </div>

        </td>
      </tr>
    </table>
  </div>

  <div class="buttons-wrapper">
    <div class="buckets-button-add js-button-add">Add bucket</div>
    <div class="buckets-button-remove js-button-remove">Remove bucket</div>
  </div>

</div>
